const path = require("path");
const Koa = require("koa");
const app = new Koa();
const staticFile = require("koa-static");
const bodyParser = require("koa-bodyparser");

const router = require("./service/router");

app.use(bodyParser());

// 静态文件
const main = staticFile(path.join(__dirname, "./views"));

app.use(router.routes());
app.use(main);

app.listen(3000, () => {
  console.log("访问127.0.0.1:3000");
});
