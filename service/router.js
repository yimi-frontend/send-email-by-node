const Router = require("koa-router");
const router = new Router();

// 路由文件引入
const routers = [
  require("./routers/checklogin"),
  require("./routers/sendemail")
];

// 创建路由
routers.forEach(({ path, method, handle }) => {
  router[method](path, handle);
});

module.exports = router;
