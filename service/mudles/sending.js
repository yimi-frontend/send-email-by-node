const nodemailer = require("nodemailer");
const sendInfo = require('../../data/sendInfo')
module.exports = async ({ subject, desc, rundeck, address }) => {
  let transporter = nodemailer.createTransport({
    host: "smtp.exmail.qq.com",
    port: 465,
    secure: true,
    auth: {
      user: sendInfo.from, // 发件人邮箱
      pass: sendInfo.pwd  // 发件人密码
    }
  });

  let res = await transporter.sendMail({
    from: sendInfo.from,  // 发件人 必须上面一样
    to: sendInfo.to,
    cc: sendInfo.cc,
    subject: `[提测]${subject}`,
    text: `${desc}`,
    html: `
      <h4>本次提测内容如下:</h4>
      <table>
        <tr><td style="text-align:right">发布域名：</td><td style="color:red">${rundeck}</td></tr>
        <tr><td style="text-align:right">需求/bug地址：</td><td style="color:red">${address}</td></tr>
        <tr><td style="text-align:right">发布环境：</td><td style="color:red">stage</td></tr>
        <tr><td style="text-align:right">内容描述：</td><td style="color:#999">${desc}</td></tr>
      </table>
      <div style="color:#00bdf3"> 温馨提示：回复邮件请回复全部 </div>
    `
  });

  return res;
};
