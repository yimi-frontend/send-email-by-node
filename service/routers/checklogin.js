module.exports = {
  path: "/checklogin",
  method: "post",
  handle(ctx, next) {
    const req = ctx.request.body;
    const DB = require("../../data/login.json");

    const check = DB.filter(
      db => db.username === req.username && db.password === req.password
    );
    if (check.length === 1) {
      // 设置cookie
      ctx.cookies.set("loginStauts", "1", {
        domain: "email-yimifudao.com", // 写cookie所在的域名
        path: "/", // 写cookie所在的路径
        maxAge: 60 * 60 * 1000, // cookie有效时长
        expires: new Date("2018-02-08"), // cookie失效时间
        httpOnly: false, // 是否只用于http请求中获取
        overwrite: false // 是否允许重写
      });
      ctx.body = {
        code: 2000,
        data: {
          userName: req.username
        },
        messsage: "SUCCESS"
      };
    } else {
      ctx.body = {
        code: 2001,
        data: null,
        messsage: "FAIL"
      };
    }
  }
};
